/*Static methods, like many other features introduced in ES6, are meant to provide class-specific methods for 
 object-oriented programming in Javascript. Static methods in Javascript are similar to class methods in Ruby.
 To declare a static method, simply prefix a method declaration with the word static inside the class declaration. */
class Number {
    static number(n = 1) {
      return n * 3;
    }
  }
  
  class BiggerNumber extends Number {
    static triple(n) {
      return super.number(n) * super.number(n);
    }
  }
  
  console.log(Number.number());        // 3
  console.log(Number.number(6));       // 18
  
  var add = new Number();
  
  console.log(BiggerNumber.number(3));
  // 9
  
 