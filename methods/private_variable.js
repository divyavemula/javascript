/* variable from outside its scope
Using # to denote privacy has received some criticism, primarily because it’s ugly and feels like a hack.
Most languages implement a private keyword, so attempting to use that member outside the class will be rejected by the compiler. */
var Names = (function() {
    const Private = new WeakMap();
    return class Names {
      constructor(name) {
        // Yes, we're only storing one value in the "private object".
        // Of course, the example is a bit contrived.
        Private.set(this, { name });
      }
  
      // For comparison to the original. I'd use 'get name()' and 
      // 'set name(name)' instead, personally, but that I think is more
      // a matter of personal preference.
      getName() { return Private.get(this).name; }
      setName(name) { Private.get(this).name = name; }
    }
  })();
  
  var name = (function(Names) {
    const Private = new WeakMap();
    return class name extends Names {
      constructor(name, job) {
        super(name);
        Private.set(this, { job });
      }
      getJob() { return Private.get(this).job; }
      setJob(job) { Private.get(this).job = job; }
  
      // Just for exposition: this should always return undefined,
      // since NamedThing has it's own WeakMap, separate from this one
      getNameFromHere() { return Private.get(this).name; }
    }
  })(Names);
  
  const data = new name('divya', 'nandhini');
  console.log(data instanceof Names); // true
  console.log(data.getName());             // divya
  console.log(data instanceof name);     // true
  console.log(data.getJob());              // nandhini
  console.log(data.getJob());              // Bone Finder