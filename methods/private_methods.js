var MyObject = (function () {

    // Constructor
    function MyObject (food) {
        this._food = food;
    }

    function privateFun (prefix) {
        return prefix + this._food;
    }

    MyObject.prototype.publicFun = function () {
        return privateFun.call(this, '');
    }

    return MyObject;
})();


var myObject = new MyObject('bar');
console.log(myObject.publicFun());  // bar
