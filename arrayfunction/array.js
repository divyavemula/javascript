  // Goal: Add two numbers and return a value
    /* i/p: 
    10, 20.60
    20, "50"
    "20", 50
    20, 50
    "C", "D" => we should reject & send them an error message
	"2CD", "CD" => we should reject & send them an error message
	"2CD", "34CD" => o/p=36
    */
   function addNumbers(operandOne, operandTwo){
    var sumOfInputs  =  0;
    var errorMessage =  {"message" : "The inputs should be valid numbers or convertible strings. Please fix & try again."}
    /*
        1. get the inputs
        2. validate the inputs
        3. implement the business logics
        4. return the output
    */ 

    // inputs are operandOne, operandTwo
    var validationResponse = validateInputs(operandOne, operandTwo);
    if(!validationResponse){
        return errorMessage;
    }
    // Business logic
    sumOfInputs = parseFloat(operandOne) + parseFloat(operandTwo);
    return sumOfInputs;
}

function validateInputs(operandOne, operandTwo){
    // small & has one responsibility

    var validationResult = true;
    // validation logic
    /* validation: 
        - the inputs should be numbers (make sure that the inputs are not strings). 
        - The strings that are convertible into numbers should be accepted
    */
    // Fix the logic
    if(typeof operandOne == "string" || typeof operandTwo == "string"){
    if( (parseFloat(operandOne) == NaN || parseFloat(operandTwo) == NaN) ){
        return false;
    }
}
    return validationResult;
}

console.log(addNumbers(10,20.60));//30.6
console.log(addNumbers(20,"50"));//70
console.log(addNumbers("20", 50));//70
console.log(addNumbers(20, 50));//70
console.log(addNumbers("20.90", 50.80));//71.6999
console.log(addNumbers("C", "D"));// var errorMessage =  {"message" : "The inputs should be valid numbers or convertible strings. Please fix & try again."}
console.log(addNumbers("2CD", "CD"));//var errorMessage =  {"message" : "The inputs should be valid numbers or convertible strings. Please fix & try again."}
console.log(addNumbers("2CD", "34CD"));//36