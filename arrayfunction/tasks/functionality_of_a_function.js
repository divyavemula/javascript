/* Can you extend a functionality of a function? How you will do, write an example. */
String.prototype.flip = function()
{
    //console.log()
    let str = this;
    let end = " ";
    let punctuation = /[/.!]/;
    let matches = str.match(punctuation); // return null or array
    if(matches)
    {
        end = matches[0];
    }

    return str
    .split('')
    .reverse()
    .join("") + end;
};
let str = "emoclew ayvid ?";
console.log(str.flip());
// now call the extended behaviour 
var data = new String();
console.log(data);
// Ans: ? divya welcome


