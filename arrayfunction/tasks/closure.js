/* Explain closure. Write an example using a cell phone. */
/* Most of the JavaScript Developers use closure consciously or unconsciously. Even if they do unconsciously
 it works fine in most of the cases. But knowing closure will provide better control over the code when using them. */

  function CellPhone(mi, nokia, samsung, oneplus ) {
    return {
      toString() {
        return "${mi} ${nokia} (${samsung}, ${oneplus})"
      }
    }
  }
  const cellPhone = new CellPhone('Aston Martin','V8 Vantage','2018','Quantum Silver')
  console.log(cellPhone.toString())

  // Ans: ${mi} ${nokia} (${samsung}, ${oneplus})