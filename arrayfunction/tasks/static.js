/*How you will access static method from a child class (the static method is in a parent class). */
class parentClass {
    static staticMethod() {
        return 'Hello';
    }

    static get staticProperty() {
        return 'Goodbye';
    }
}

// using static methods
console.log(parentClass.staticMethod()); //  "Hello"
console.log(parentClass.staticProperty); //  "Goodbye"

// just like you expect, Static properties are not defined on object instances:

const parentClassInstance = new parentClass();
console.log(parentClassInstance.staticProperty);  logs: undefined

//However, they are defined on subclasses:
class childClass extends parentClass {};

console.log(childClass.staticMethod()); // "Hello"
console.log(childClass.staticProperty); //  "Goodbye"