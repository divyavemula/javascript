/* Create a class called 'fan', assume your own data members, methods atleast 6 each and two private data members. */

//class declaration
class fans {
    constructor(steam=20, electrical=12, noise=6, exhaustFan =1, fanMotorDriveMethods=24, crossFlow=30, )
    { 
      var _fan = 6;
      var brand ="sonic"
      this.steam   = steam;
      this.electrical = electrical;
      this.noise     = noise ;
      this.exhaustFan     = exhaustFan;
      this.fanMotorDriveMethods  = fanMotorDriveMethods  ;
      this.crossFlow   = crossFlow ;
    }
    calculateEstimate(){
        this.cost = this.steam * this.electrical * this.noise * this.exhaustFan * this.fanMotorDriveMethods * this.crossFlow;
        return this.cost;
    }
}

var myFan = new fans(); // Default values
var _cost   = myFan.calculateEstimate()
console.log(_cost);

    /*Create a child class 'tableFan' from above 'fan' */

    // Creating a new class from the parent
class tableFan extends fans {
    }
    
    var _tableFan = new tableFan();
    console.log(tableFan.steam);
    _tableFan.calculateEstimate();
        // calling super class's method
        _tableFan.saleAgreement // calling undefined valve

/* Create a child class 'ceilingFan' from 'fan' */    
class ceilingFan extends fans {
}

var _ceilingFan = new ceilingFan();
console.log(ceilingFan.steam);
_ceilingFan.calculateEstimate();
    // calling super class's method
    _ceilingFan.saleAgreement 

/*  Try to access private members from 'fan' in 'ceilingFan'. Is this allowed?*/

class towerFan extends fans{
    /*
    child: 'tablefan'
    parent: 'fan'
    */
       
        getFanBrand()
        {
                return "The fan Brand is : " + myFan.brand;
        }
    }
    
var mytowerFan = new towerFan();
var _mytowerFan = mytowerFan.getFanBrand();
console.log( _mytowerFan);
/*ans:1036800
undefined
VM81:41 undefined
57600 */



