function Person(name, age) {
    this.age = age;
    this.name = name;
    this.getInfo = function() {
        return "I am " + this.age + " years old " +
        "and weighs " + this.name +" name.";
    }
}
function Employee(age, name, salary) {
    this.salary = salary;
    this.age = age;
    this.name = name;
    this.getInfo = function() {
        return "I am " + this.age + " years old " +
        "and weighs " + this.name +" name" +
        "and earns " + this.salary + " dollar.";
    }
}

Employee.prototype = new Person();
Employee.prototype.constructor = Employee;
  // The argument, 'obj', can be of any kind
  // which method, getInfo(), to be executed depend on the object
  // that 'obj' refer to.

function showInfo(obj) {
    document.write(obj.getInfo() + "<br>");
}

var person = new Person(50,"divya");
var employee = new Employee(43,"divya",50000);
console.log(person);
console.log(employee);
/*ans:person { age: 'divya', name: 50, getInfo: [Function (anonymous)] }
Employee {
  salary: 50000,
  age: 43,
  name: 'divya',
  
} */