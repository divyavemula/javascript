function Person() {
    //properties/fields
    var name = "divya";
    var height = 5.3;
    var weight = 50;
    var socialInsuranceNumber = "555 555 555";
  
    return {
      setHeight: function(newHeight) {height=newHeight;},
      getHeight: function() { return height; },
      setWeight: function(newWeight) {weight = newWeight;},
      getWeight: function() { return weight; },
      setName:   function(newName) {name=newName;},
      getName:   function() { return name; },
      setSocialInsuranceNumber: function(newSocialInsuranceNumber) { socialInsuranceNumber=newSocialInsuranceNumber; }
    };
  }
  
  //instanciate the Person class
  var aPerson = new Person();
  var myName = aPerson.getName();
  var myHeight = aPerson.getHeight();
  console.log(myName); 
  // ans: divya
  console.log(myHeight);//5.3