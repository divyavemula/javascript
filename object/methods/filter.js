//Declare an array of numbers
//syntax:array.filter(function(currentValue, index, arr), thisValue)
//filter() method creates an array filled with all array elements that pass a test
var data = [
	{name: "divya", franchise: "software"},
	{name: "nandhini", franchise: "software"},
	{name: "moni", franchise: "Marvel"},
	{name: "teju", franchise: "actor"}
];

var marvelHeroes =  data.filter(function(data) {
	return data.franchise == "software";
});
console.log(marvelHeroes);