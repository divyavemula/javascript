// syntax: array.every(function(currentValue, index, arr), thisValue)
// every() method checks if all elements in an array pass a test
//Declare an array of values
let value = [10, 5, 20, 100].every(function(number){
    return number < 150
})
console.log(value)