//syntax:array.reduce(function(total, currentValue, currentIndex, arr), initialValue)
/*The reduce() method reduces the array to a single value.
The reduce() method executes a provided function for each value of the array (from left-to-right).
The return value of the function is stored in an accumulator (result/total). */
//Declare an array of numbers

var array = [1,2,3,4,5];

//Declare an answer variable to store the final result
//The reduce function callback is declared in the declaration
let data = array.reduce((totalResult,currValue)=>
//The callback method must return a value using the required function       
             {return totalResult*currValue;});
console.log(" initial value is : " +data)


