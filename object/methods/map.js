//syntax:array.map(function(currentValue, index, arr), thisValue)
// map() method creates a new array with the results of calling a function for every array element.
let dataMap = new Map()
dataMap['bla'] = 'blaa'
dataMap['bla2'] = 'blaaa2'
dataMap['bla2'] = 'blaaa2'
dataMap['b1'] = 'bla2'

console.log(dataMap)