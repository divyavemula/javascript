/* Anonymous Functions */
//An anonymous function is a function without a name
var myVariable = "I live outside the function.";
(function() {
 var myVariable = "I live in this anonymous function";
 console.log(myVariable);
})();
console.log(myVariable);
//Arrow functions
//ES6 introduced arrow function expression that provides a shorthand for declaring anonymous functions
let show = function () {
    console.log('Anonymous function');
};
//can be shortened using the following arrow function
let show = () => console.log('Anonymous function');
//Similarly, the following anonymous function:

let add = function (a, b) {
    return a + b;
};
//is equivalent to the following arrow function:

let add = (a, b)  => a + b;  
/*Immediately invoked function execution
If you want to create a function and execute it immediately after declaration, you can use the anonymous function like this:*/

(function() {
    console.log('IIFE');
})();
//How it works.

//First, the following defines a function expression:

(function () {
    console.log('Immediately invoked function execution');
})
//Second, the trailing parentheses () allow you to call the function:

(function () {
    console.log('Immediately invoked function execution');
})();
//and sometimes, you may want to pass arguments into it, like this:

let person = {
    firstName: 'John',
    lastName: 'Doe'
};

(function () {
    console.log("${person.firstName} ${person.lastName}");
})(person);