/*What happens when you access an element outside the length of the string with charAt? */
/*What happens when you do the same thing with the square brackets?*/
var name = "divya";
undefined
name.length
5
name.charAt(6)
""
name.charAt(5)
""
name.charAt(4)
"a"

name[4]
"a"
name[6]
undefined