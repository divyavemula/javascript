/ * Create two arrays of vowels, integers (0 to 9). Print the combination of these two arrays.*/
var vowels = ["a","e","i","o","u"]
var numbers = [0,1,2,3,4,5,6,7,8,9]


var combination = vowels.reduce(function(a, v, i) {
    a = a.concat(numbers.map(function(w){
      return v + w
    }));
    return a;
    },[]
);

console.log(combination);

