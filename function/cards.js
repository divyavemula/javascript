/*
Practice 1. Using Chrome's JavaScript console, write a function that takes a rank and a suit as input, 
and returns a string representation of a card. Use it to print out a few cards with various suits and ranks. 
*/

function Cards(rank, suit) {

    this.rank = rank;
    this.suit = suit;
  
    this.toString  = cardToString;
  }

function cardToString() {

var rank, suit;
  
switch (this.rank) {
case "1" :
    rank = "A";
    break;
case "2" :
    rank = "2";
    break;
case "3" :
    rank = "3";
    break;
case "4" :
    rank = "4";
    break;
case "5" :
    rank = "5";
    break;
case "6" :
    rank = "6";
    break;
case "7" :
    rank = "7";
    break;
case "8" :
    rank = "8";
    break;
case "9" :
    rank = "9";
    break;
case "10" :
    rank = "10";
    break;
case "J" :
    rank = "J"
    break;
case "Q" :
    rank = "Q"
    break;
case "K" :
    rank = "K"
    break;
default :
    break;
}     

switch (this.suit) {
case "C" :
    suit = "Clubs";
    break;
case "D" :
    suit = "Diamonds"
    break;
case "H" :
    suit = "Hearts"
    break;
case "S" :
    suit = "Spades"
    break;
default :
    break;
}

// 1. validate inputs

if (rank == null || suit == null)
{
    return "";
}        
// 2. implement business logic
// 3. respond (return a value)      
return rank + " of " + suit;
}

var myCard = new Cards("1" , "S");
console.log(myCard.toString());

