
 function addThree (firstNum, secondNum, thirdNum) {
    var resultOfSum    = 0;
    // 1. validate inputs
            var validationResponse = validateInputs(firstNum,secondNum,thirdNum);
            if(!validationResponse){
                return { "message": "Invalid Inputs. This function accepts only integers." }
            }        
    // 2. implement business logic
            resultOfSum    = parseFloat(firstNum) + parseFloat(secondNum) +parseInt(thirdNum)
    // 3. respond (return a value)
            return resultOfSum;        

}

function validateInputs(firstNum, secondNum,thirdNum){
    // The inputs should be integers (10, 20) or doubles (10.5, 20.6).
    // Edge scenario: "10.5", "20". In this case, we should "typecast" (converting one type to another type)
        if(typeof firstNum == "string" || typeof secondNum == "string" || typeof thirdNum == "string")
        {
            if(parseFloat(firstNum) == NaN || parseFloat(secondNum) == NaN || typeof thirdNum == NaN)
            {
                return false;
            }
        }
        return true;
}
console.log(addThree(10,20,20));
   // return firstNum + secondNum + thirdNum;

  