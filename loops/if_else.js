var greetingByHour = function (hour) {
    var result; 
    if (0 <= hour && hour <= 5) {
    result = "Wow, it's early!";
    } else if (5 < hour && hour <= 12) {
    result = "Good Morning!";
    } else if (12 < hour && hour <= 17) {
    result = "Good Afternoon!";
    } else if (17 < hour && hour <= 20) {
    result = "Good Evening!";
    } else if (20 < hour && hour <= 24) {
    result = "Shouldn't you be in bed?";
    } else {
    result = "Oh gosh, this is awkward -- that's not a time.";
    }
    return result;
   }
   console.log(greetingByHour);
   