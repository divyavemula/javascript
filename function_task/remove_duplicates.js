/*Write a function that converts an array of array to an array of strings without duplicates.

input: [ [10, 20],  ["10",20], ["A20", 20] , ["ABCD20", 20] ]
output (order does not matter):  ["10", "A20", "20" , "ABCD20"]*/

let chars = [ [10, 20],  ["10",20], ["A20", 20] , ["ABCD20", 20] ];
var singleArray  = chars.flat(); 
var array  = [].concat(...chars);
let uniqueChars = [...new Set(chars)];
console.log("The array of array converted into single array : " + singleArray); 

var Array = singleArray.map(String);
console.log(Array)
console.log("The array converted into string : " + Array);

function getUnique(array){
    var uniqueArray = []
    
    for(i=0; i < array.length; i++){
        if(uniqueArray.indexOf(array[i]) === -1) {
            uniqueArray.push(array[i]);
        }
    }
    return uniqueArray;
}
console.log("The  array without duplicates are : " + getUnique(Array));