/*3. Write a function that calculates and returns the area square of an array of  rectangles. 
Formula: Area Square = length * width.
Input:
rectangles = [ [10, 20],  ["10",20], ["A20", 20] , ["ABCD20", 20] ]

Expected output: [ 200, 200,  400, 400] */

var input = [ [10, 20],  ["10",20], ["A20", 20] , ["ABCD20", 20] ];
var output =input.reduce(function(returnValue, next, index) {
var length = Number(String(next[0]).replace(/\D/g,''));
var width = Number(String(next[1]).replace(/\D/g,''));                                 
rectangle = length * width;
return returnValue;
},[]);                                
alert(output)

